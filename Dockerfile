FROM python:3.6-stretch

RUN curl -Lo kops https://github.com/kubernetes/kops/releases/download/$(curl -s https://api.github.com/repos/kubernetes/kops/releases/latest | grep tag_name | cut -d '"' -f 4)/kops-linux-amd64 \
  && chmod +x ./kops \
  && mv ./kops /usr/local/bin/

RUN  curl -Lo kubectl https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl \
  && chmod +x ./kubectl \
  && mv ./kubectl /usr/local/bin/kubectl

RUN  pip install --upgrade pip

RUN pip install awscli --upgrade

RUN pip install aws-sam-cli

WORKDIR /local

# ENTRYPOINT ["sam"]
